This plugin for Spigot API 1.19 servers is designed to increase the difficulty and realism of crop and tree production. 

Examples:

- The growth of trees and crops is affected by biome compatibility. They will die outside of their native ranges, and grow more slowly in certain biomes.
- When trees grow, they can spawn seedlings in their vicinity, causing forests to spread on their own;
- Certain weather conditions can kill certain unsheltered plants;
- Wheat seeds are no longer dropped. Instead, you can craft them from wheat items;
- Harvesting a crop will cause the soil to become untilled again.
- Weeds will pop up on their own. This causes grasses, flowers, and seaweeds to populate their appropriate habitats over time.
- Certain plants require full sun, while others require some shade. The wrong conditions will lead to death.
- Some plants will just die as they attempt to reach maturity.

All of this is governed by a highly tweakable config file. The default settings are meant to mirror the Vanilla Minecraft ecology, while increasing realism making it much harder to cultivate useful plants. Patience, trial, and error will become necessary for your players' survival.

Each time a plant attempts to grow, condition checks and a dice roll will decide whether it dies, waits, or succeeds. As a result, wheat might grow roughly 21 times more slowly under ideal conditions.
