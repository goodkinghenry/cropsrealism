package digital.amaranth.mc.cropsrealism;

import org.bukkit.Bukkit;

import org.bukkit.event.HandlerList;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class CropsRealism extends JavaPlugin {
    public static CropsRealism getInstance() {
        Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("CropsRealism");
        if (plugin == null || !(plugin instanceof CropsRealism)) {
            throw new RuntimeException("'CropsRealism' not found.");
        }
        
        return ((CropsRealism) plugin);
    }

    @Override
    public void onEnable() {
        saveDefaultConfig();
        var c = this.getConfig();
        
        if (c.getBoolean("EnableWheatRecipes")) {
            Recipes recipes = new Recipes(c.getInt("WheatSeedsPerWheat"));
            recipes.loadRecipes();
        }
        
        getServer().getPluginManager().registerEvents(
                new CropGrowth(
                        c.getBoolean("EnableWheatRecipes"),
                        c.getBoolean("EnableFarmlandFallowing"),
                        c.getDouble("FrostDeathOdds"),
                        c.getDouble("StormDeathOdds"),
                        c.getDouble("RandomDeathOdds"),
                        c.getDouble("SunlightDeathOdds"),
                        c.getDouble("DarknessDeathOdds"),
                        c.getDouble("GrowthRateModifier"),
                        c.getDouble("TreeGrowthRateModifier"),
                        c.getDouble("WeedsGrowthOdds"),
                        c.getInt("SaplingSpreadRadius"),
                        c.getInt("SaplingLowerBound"),
                        c.getInt("SaplingUpperBound"),
                        c.getInt("WeedsSpreadRadius"),
                        c.getInt("WeedsLowerBound"),
                        c.getInt("WeedsUpperBound"),
                        c.getMapList("BiomeParameters"),
                        c.getList("NeedsFullSun"),
                        c.getList("NeedsShade"),
                        c.getList("SurvivesFrost"),
                        c.getList("SurvivesDarkness"),
                        c.getList("WeedsList"),
                        c.getList("AquaticWeedsList")
        ), this);
    }
    
    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
