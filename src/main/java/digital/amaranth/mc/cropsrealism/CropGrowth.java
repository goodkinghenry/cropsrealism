package digital.amaranth.mc.cropsrealism;

import static digital.amaranth.mc.quickblocklib.Blocks.Categories.isABush;
import static digital.amaranth.mc.quickblocklib.Blocks.Categories.isAPlant;
import static digital.amaranth.mc.quickblocklib.Blocks.Categories.isATreeSapling;
import static digital.amaranth.mc.quickblocklib.Blocks.Categories.isAnAquaticPlant;
import static digital.amaranth.mc.quickblocklib.Blocks.Categories.isAquaticSoil;
import static digital.amaranth.mc.quickblocklib.Blocks.Categories.isSoil;

import static digital.amaranth.mc.quickblocklib.Blocks.Conditions.isHighest;

import static digital.amaranth.mc.quickblocklib.Blocks.Switches.getSaplingByTreeType;

import static digital.amaranth.mc.quickblocklib.Weather.Conditions.getSunlight;
import static digital.amaranth.mc.quickblocklib.Weather.Conditions.isDayTime;
import static digital.amaranth.mc.quickblocklib.Weather.Conditions.isInAThunderstorm;
import static digital.amaranth.mc.quickblocklib.Weather.Conditions.isInSnow;

import static digital.amaranth.mc.quickblocklib.Weather.Constants.MAX_ICE_FORMING_LIGHT_LEVEL;
import static digital.amaranth.mc.quickblocklib.Weather.Constants.MIN_DAYLIGHT_LIGHT_LEVEL;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Levelled;
import org.bukkit.event.EventHandler;

import org.bukkit.event.Listener;

import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFertilizeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.world.StructureGrowEvent;

public class CropGrowth implements Listener {
    private final boolean ENABLE_WHEAT_RECIPES,
            ENABLE_FARMLAND_FALLOWING;
    
    private final double FROST_DEATH_ODDS,
            STORM_DEATH_ODDS,
            RANDOM_DEATH_ODDS,
            SUNLIGHT_DEATH_ODDS,
            DARKNESS_DEATH_ODDS,
            GROWTH_RATE_MODIFIER,
            TREE_GROWTH_RATE_MODIFIER,
            WEEDS_GROWTH_ODDS;
    
    private final int SAPLING_SPREAD_RADIUS,
            MIN_SAPLINGS,
            MAX_SAPLINGS,
            WEEDS_SPREAD_RADIUS,
            MIN_WEEDS,
            MAX_WEEDS;
    
    private final List<Map<?, ?>> BIOME_PARAMETERS;
    
    private final List<?> NEEDS_FULL_SUN,
            NEEDS_SHADE,
            SURVIVES_FROST,
            SURVIVES_DARKNESS,
            WEEDS_LIST,
            AQUATIC_WEEDS_LIST;
    
    public CropGrowth (boolean enableWheatRecipes,
            boolean enableFarmlandFallowing,
            double frostDeathOdds,
            double stormDeathOdds,
            double randomDeathOdds,
            double sunlightDeathOdds,
            double darknessDeathOdds,
            double growthRateModifier,
            double treeGrowthRateModifier,
            double weedsGrowthOdds,
            int saplingSpreadRadius,
            int minSaplings,
            int maxSaplings,
            int weedsSpreadRadius,
            int minWeeds,
            int maxWeeds,
            List<Map<?, ?>> biomeParameters,
            List<?> needsFullSun,
            List<?> needsShade,
            List<?> survivesFrost,
            List<?> survivesDarkness,
            List<?> weedsList,
            List<?> aquaticWeedsList) {
        this.ENABLE_WHEAT_RECIPES = enableWheatRecipes;
        this.ENABLE_FARMLAND_FALLOWING = enableFarmlandFallowing;
        this.FROST_DEATH_ODDS = frostDeathOdds;
        this.STORM_DEATH_ODDS = stormDeathOdds;
        this.RANDOM_DEATH_ODDS = randomDeathOdds;
        this.SUNLIGHT_DEATH_ODDS = sunlightDeathOdds;
        this.DARKNESS_DEATH_ODDS = darknessDeathOdds;
        this.GROWTH_RATE_MODIFIER = growthRateModifier;
        this.TREE_GROWTH_RATE_MODIFIER = treeGrowthRateModifier;
        this.WEEDS_GROWTH_ODDS = weedsGrowthOdds;
        this.SAPLING_SPREAD_RADIUS = saplingSpreadRadius;
        this.MIN_SAPLINGS = minSaplings;
        this.MAX_SAPLINGS = maxSaplings;
        this.WEEDS_SPREAD_RADIUS = weedsSpreadRadius;
        this.MIN_WEEDS = minWeeds;
        this.MAX_WEEDS = maxWeeds;
        
        this.BIOME_PARAMETERS = biomeParameters;
        this.NEEDS_FULL_SUN = needsFullSun;
        this.NEEDS_SHADE = needsShade;
        this.SURVIVES_FROST = survivesFrost;
        this.SURVIVES_DARKNESS = survivesDarkness;
        this.WEEDS_LIST = weedsList;
        this.AQUATIC_WEEDS_LIST = aquaticWeedsList;
    }
    
    private final Random r = new Random();
    
    private boolean rollForDeath (Block b, double odds) {
        if (r.nextDouble() < odds) {
            handleDeath(b);
            
            return true;
        } else {
            return false;
        }
    }
    
    private void handleDeath (Block b) {
        if (isABush(b) || isATreeSapling(b)) {
            b.setType(Material.DEAD_BUSH);
        } else if (isAnAquaticPlant(b)) {
            b.setType(Material.WATER);
        } else if (isInSnow(b)) {
            b.setType(Material.SNOW);
        } else if (b.getType().equals(Material.GRASS_BLOCK)) {
            b.setType(Material.DIRT);
        } else if (!isSoil(b) && !isAquaticSoil(b) && !b.getType().equals(Material.MOSS_BLOCK)) {
            b.setType(Material.AIR);
        }
    }
    
    private double getGrowthOdds (String t, String B) {
        for (var M : BIOME_PARAMETERS) {
            for (var A : M.entrySet()) {
                String species = (String)A.getKey();
                
                if (species.equals(t)) {
                    for (var m : (ArrayList)A.getValue()) {
                        for (var e : ((LinkedHashMap)m).entrySet()) {
                            String biome = (String)((Entry)e).getKey();
                            
                            if (biome.equals(B)) {
                                var growthRate = (double)((Entry)e).getValue();
                                
                                return growthRate;
                            }
                        }
                    }
                    
                    return 0; // do not grow if species found but biome not
                }
            }
        }
        
        return -1; // return -1 to signal that the species is absent
    }
    
    private boolean needsFullSun (String s) {
        return NEEDS_FULL_SUN.contains(s);
    }
    
    private boolean needsShade (String s) {
        return NEEDS_SHADE.contains(s);
    }
    
    private boolean survivesDarkness (String s) {
        return SURVIVES_DARKNESS.contains(s);
    }
    
    private boolean survivesFrost (String s) {
        return SURVIVES_FROST.contains(s);
    }
    
    private int getPoint (int radius) {
        return (r.nextBoolean() ? 1 : -1) * r.nextInt(0, radius);
    }
    
    private boolean plantOnRandomBlock (Location l, int radius, Material t, boolean aquatic) {
        boolean hasSucceeded = false;
        
        int MAX_BLOCK_SEEK_TRIES = 5, MAX_VERTICAL_SEEK_DISTANCE = 20;

        for (int i=0; i<MAX_BLOCK_SEEK_TRIES; i++) {
            var W = l.getWorld();

            if (W != null) {
                Block h = W.getHighestBlockAt(l.getBlockX() + getPoint(radius), l.getBlockZ() + getPoint(radius));

                if (aquatic) {
                    while (h.getType().equals(Material.WATER)) { // seek down to the marine soil
                        h = h.getRelative(BlockFace.DOWN);
                    }

                    if (isAquaticSoil(h) && h.getRelative(BlockFace.UP).getType().equals(Material.WATER)) {
                    } else {
                        continue;
                    }
                } else {
                    if (isSoil(h) && h.getRelative(BlockFace.UP).getType().isAir()) {
                    } else {
                        continue;
                    }
                }

                if (Math.abs(h.getY() - l.getY()) < MAX_VERTICAL_SEEK_DISTANCE) {
                    h.getRelative(BlockFace.UP).setType(t);

                    hasSucceeded = true;
                }
            }
        }
        
        return hasSucceeded;
    }
    
    private void plantOnRandomBlocks (Location l, int radius, Material t, int n, boolean aquatic) {
        for (int i=0; i<n; i++) {
            plantOnRandomBlock(l, radius, t, aquatic);
        }
    }
    
    public void plantSaplings (Location l, Material t) {
        int n = MIN_SAPLINGS + r.nextInt(MAX_SAPLINGS - MIN_SAPLINGS);
        
        if (t.equals(Material.MANGROVE_PROPAGULE)) {
            plantOnRandomBlocks(l, SAPLING_SPREAD_RADIUS, t, n/2, false);
            plantOnRandomBlocks(l, SAPLING_SPREAD_RADIUS, t, n/2, true);
        } else {
            plantOnRandomBlocks(l, SAPLING_SPREAD_RADIUS, t, n, false);
        }
    }
    
    private void plantWeed (Location l, List<?> L, boolean aquatic) {
        int i = r.nextInt(L.size());
        
        plantOnRandomBlock(l, WEEDS_SPREAD_RADIUS, Material.valueOf((String)L.get(i)), aquatic);
    }
    
    private void plantWeeds (Location l, List<?> L, boolean aquatic) {
        int n = MIN_WEEDS + r.nextInt(MAX_WEEDS - MIN_WEEDS);
        
        for (int i=0; i<n; i++) {
            plantWeed(l, L, aquatic);
        }
    }
    
    private List<?> prunePlantListToBiomeCompatible (List<?> L, String B) {
        return L.stream().filter(w -> getGrowthOdds((String)w, B) > 0).toList();
    }
    
    private boolean rollForWeeds (Location l, double odds) {
        List<?> modifiedWeedsList;
        
        Block b = l.getBlock();
        String B = b.getBiome().toString();
        
        boolean aquatic = false;
        if (isAnAquaticPlant(b)) { // if the originating block is an aquatic plant, then grow aquatic weeds
            modifiedWeedsList = prunePlantListToBiomeCompatible(AQUATIC_WEEDS_LIST, B);
            
            aquatic = true;
        } else {
            modifiedWeedsList = prunePlantListToBiomeCompatible(WEEDS_LIST, B);
        }
        
        if (!modifiedWeedsList.isEmpty() && (r.nextDouble() < odds)) {
            plantWeeds(l, modifiedWeedsList, aquatic);
            
            return true;
        } else {
            return false;
        }
    }
    
    private boolean doesBlockGrow (Block b, String t) {
        return doesBlockGrow(b, t, 1);
    }
    
    private boolean doesBlockGrow (Block b, String t, double m) {
        double growthOdds = m * getGrowthOdds(t, b.getBiome().toString());
        
        if (growthOdds < 0) { // there are no rules for this object, so do nothing
            return true;
        } else if (growthOdds == 0) { // zero means the crop can not grow here
            handleDeath(b);
            
            return false;
        } else {
            if (isHighest(b)) { // is the crop exposed to the elements?
                if (isInSnow(b) && !survivesFrost(t)) {
                    rollForDeath(b, FROST_DEATH_ODDS);
                    
                    return false;
                } else if (isInAThunderstorm(b)) {
                    rollForDeath(b, STORM_DEATH_ODDS);
                    
                    return false;
                }
            }
            
            boolean survivesDarkness = survivesDarkness(t);
            
            // only dark-surviving crops may grow at night or during storms
            if (survivesDarkness || (isDayTime(b) && b.getWorld().isClearWeather())) {
                int sunlightLevel = getSunlight(b);
                
                if (sunlightLevel > MIN_DAYLIGHT_LIGHT_LEVEL) { // full sun
                    if (needsShade(t)) {
                        rollForDeath(b, SUNLIGHT_DEATH_ODDS);
                        
                        return false;
                    }
                } else if (sunlightLevel < MAX_ICE_FORMING_LIGHT_LEVEL) { // darkness
                    if (!survivesDarkness) {
                        rollForDeath(b, DARKNESS_DEATH_ODDS);

                        return false;
                    }
                } else { // shade
                    if (needsFullSun(t)) {
                        rollForDeath(b, DARKNESS_DEATH_ODDS);

                        return false;
                    }
                }
                
                double roll = r.nextDouble();
                double finalOdds = growthOdds * GROWTH_RATE_MODIFIER;

                if (roll < finalOdds) {
                    // at each successful growth roll, roll for a random death instead
                    if (!rollForDeath(b, RANDOM_DEATH_ODDS)) {
                        rollForWeeds(b.getLocation(), WEEDS_GROWTH_ODDS);

                        return true;
                    }
                }
            }
        }
        
        return false;
    }
    
    /**
     * converts farmland back to dirt after a crop is harvested
     * @param e
     */
    @EventHandler  (ignoreCancelled = true)
    public void onBlockBreakEvent (BlockBreakEvent e) {
        Block b = e.getBlock();
        
        if (ENABLE_FARMLAND_FALLOWING && !b.getType().isSolid() && isAPlant(b)) {
            Block u = b.getRelative(BlockFace.DOWN);
            
            if (u.getType().equals(Material.FARMLAND)) {
                u.setType(Material.DIRT);
            }
        }
    }
    
    /**
     * prevents wheat seed drops if EnableWheatRecipes is true
     * @param e
     */
    @EventHandler (ignoreCancelled = true)
    public void onItemSpawnEvent (ItemSpawnEvent e) {
        Material itemType = e.getEntity().getItemStack().getType();
        
        if (ENABLE_WHEAT_RECIPES) {
            switch (itemType) {
                case WHEAT_SEEDS:
                    e.setCancelled(true);
                default:
            }
        }
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockGrowEvent (BlockGrowEvent e) {
        Block b = e.getBlock();
        
        e.setCancelled(!doesBlockGrow(b, b.getType().toString()));
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockFormEvent (BlockFormEvent e) {
        Block b = e.getNewState().getBlock();
        
        e.setCancelled(!doesBlockGrow(b, b.getType().toString()));
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockSpreadEvent (BlockSpreadEvent e) {
        Block b = e.getSource();
        
        e.setCancelled(!doesBlockGrow(b, b.getType().toString()));
    }
    
    @EventHandler (ignoreCancelled = true)
    public void onBlockFertilizeEvent (BlockFertilizeEvent e) {
        Block b = e.getBlock();
        
        e.setCancelled(!doesBlockGrow (b, b.getType().toString()));
    }
    
    /**
     * handles growth of trees and the production of volunteer saplings
     * @param e
     */
    @EventHandler (ignoreCancelled = true)
    public void onStructureGrowEvent (StructureGrowEvent e) {
        Location l = e.getLocation();
                
        if (doesBlockGrow(l.getBlock(), e.getSpecies().toString(), TREE_GROWTH_RATE_MODIFIER)) {
            plantSaplings(l, getSaplingByTreeType(e.getSpecies()));
        } else {
            e.setCancelled(true);
        }
    }
}
