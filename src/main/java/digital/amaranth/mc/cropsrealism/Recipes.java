package digital.amaranth.mc.cropsrealism;

import static digital.amaranth.mc.cropsrealism.CropsRealism.getInstance;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapelessRecipe;

/**
 * this class makes it possible to craft wheat seeds from wheat and vice versa
 * @author goodkinghenry
 */
public class Recipes {
    private final String foodGroupKey;
    private final int WHEAT_SEEDS_PER_WHEAT;
    
    public Recipes (int wheatSeedsPerWheat) {
        this.foodGroupKey = "food_recipes";
        this.WHEAT_SEEDS_PER_WHEAT = wheatSeedsPerWheat;
    }
    
    private void loadWheatSeedsRecipe () {
        ItemStack wheat = new ItemStack(Material.WHEAT_SEEDS, WHEAT_SEEDS_PER_WHEAT);
        NamespacedKey namespacedKey = new NamespacedKey(getInstance(), "winnowed_wheat_seeds");
        ShapelessRecipe recipe = new ShapelessRecipe(namespacedKey, wheat);
        recipe.setGroup(foodGroupKey);
        recipe.addIngredient(1, Material.WHEAT);
        Bukkit.addRecipe(recipe);
    }
    
    private void loadWheatSkeinRecipe () {
        ItemStack wheat = new ItemStack(Material.WHEAT);
        NamespacedKey namespacedKey = new NamespacedKey(getInstance(), "bundled_wheat");
        ShapelessRecipe recipe = new ShapelessRecipe(namespacedKey, wheat);
        recipe.setGroup(foodGroupKey);
        recipe.addIngredient(WHEAT_SEEDS_PER_WHEAT, Material.WHEAT_SEEDS);
        Bukkit.addRecipe(recipe);
    }
    
    public void loadRecipes () {
        loadWheatSeedsRecipe();
        loadWheatSkeinRecipe();
    }
}
